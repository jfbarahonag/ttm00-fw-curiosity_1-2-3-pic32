/* 
 * File:   pic32_uart.h
 * Author: user
 *
 * Created on May 31, 2017, 12:01 PM
 */

#ifndef _PIC32_UART_H
#define	_PIC32_UART_H

#ifndef SYS_FREQ
#define SYS_FREQ			(96000000UL)
#endif 
#ifndef PBCLK_FREQUENCY
#define PBCLK_FREQUENCY		(96 * 1000 * 1000)
#endif

#define PRINT_MESSAGE(ARG1, ARG2)  sprintf(U1TxBuf, ARG1, ARG2);\
                    UART1_write_string(U1TxBuf)

#define PRINT_MESSAGE_SIMPLE(ARG1)  sprintf(U1TxBuf, ARG1);\
                    UART1_write_string(U1TxBuf)

#define U1RxBufSize 64
#define U1TxBufSize 64

extern unsigned char U1TxBuf[U1TxBufSize];
extern unsigned char U1RxBuf[U1RxBufSize];
extern unsigned char U1Unread;

void UART1_init(unsigned long int UART_Baud);
void UART1_write_string(const char *string);
void UART1_write_char(const char ch);

void UART1_reset_rx_buffer(void);
unsigned char UART1_is_unread(void);
void UART1_clear_unread(void);
void UART1_get_string(void);


#endif	/* UART_H */

