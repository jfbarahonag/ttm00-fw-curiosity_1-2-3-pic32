#include<stdio.h>
#include<stdlib.h>
#include<stdint.h>
#include"main.h"

#define SYS_FREQ        (96000000UL)
#define PBCLK_FREQUENCY        (96 * 1000 * 1000)
#define TICKS_PER_SEC   1
#define CORE_TICK_RATE  (SYS_FREQ/2/TICKS_PER_SEC)


enum{
    LED_OFF,
    LED_ON
};

enum{
    LED_1,
    LED_2,
    LED_3
};

void main(void){
    OpenCoreTimer(CORE_TICK_RATE);
    mConfigIntCoreTimer(CT_INT_ON | CT_INT_PRIOR_2);
    INTEnableSystemMultiVectoredInt();
    //Sw
    mPORTDSetPinsDigitalIn(BIT_6);
    //Cx
    UART1_init(115200);
    UART1_write_string("[PIC]: Initializing...\r\n");
    // PORTB
    mPORTBSetPinsDigitalOut(BIT_2|BIT_3|BIT_10);
    // PORTE
    mPORTESetPinsDigitalOut(BIT_4|BIT_6|BIT_7);
    
    LED_on(LED_1);
    LED_off(LED_2);
    LED_on(LED_3);
    
    while(1){
        /*if(PORTDbits.RD6){
            LED_off(LED_1);
        }
        else{
            LED_on(LED_1);
        }*/
    }
}

void LED_on(uint8_t LED){
    switch (LED){
        case LED_1:
            mPORTESetBits(BIT_4);
            break;
        case LED_2:
            mPORTESetBits(BIT_6);
            break;
        case LED_3:
            mPORTESetBits(BIT_7);
            break;
        default:
            break;
    }
}

void LED_off(uint8_t LED){
    switch (LED){
        case LED_1:
            mPORTEClearBits(BIT_4);
            break;
        case LED_2:
            mPORTEClearBits(BIT_6);
            break;
        case LED_3:
            mPORTEClearBits(BIT_7);
            break;
        default:
            break;
    }
}

void LED_toggle(uint8_t LED){
    switch (LED){
        case LED_1:
            mPORTEToggleBits(BIT_4);
            break;
        case LED_2:
            mPORTEToggleBits(BIT_6);
            break;
        case LED_3:
            mPORTEToggleBits(BIT_7);
            break;
        default:
            break;
    }
}

void __ISR(_CORE_TIMER_VECTOR, ipl2) _CoreTimerHandler(void)
{
    LED_toggle(LED_1);
    // clear the interrupt flag
    mCTClearIntFlag();
    // update the period
    UpdateCoreTimer(CORE_TICK_RATE);
}